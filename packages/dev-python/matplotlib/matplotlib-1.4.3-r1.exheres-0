# Copyright 2011 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.gz ]

SETUP_CFG_COMMANDS="gui_support"

SETUP_CFG_gui_support_PARAMS=(
    "agg = True"
)

SETUP_CFG_gui_support_OPTIONS=(
    "cairo cairo True False"
    "gtk gtk True False"
    "gtkagg gtk True False"
    "gtk3agg gtk3 True False"
    "qt4agg qt4 True False"
    "qt5agg qt5 True False"
    "tkagg tk True False"
    "wxagg wxwidgets True False"
)

setup-cfg_gui_support_append() {
    echo gtk3cairo = $(option cairo $(option gtk3 True False) False) >> setup.cfg
}

require setup-py [ import=setuptools cfg_file=setup python_opts='[tk?]' ]

SUMMARY="Matplotlib is a python 2D plotting library."
DESCRIPTION="
Matplotlib produces publication quality figures in a variety of hardcopy formats and interactive
environments across platforms. matplotlib can be used in python scripts, the python and ipython
shell (ala MATLAB or Mathematica), web application servers, and six graphical user interface
toolkits.

matplotlib tries to make easy things easy and hard things possible. You can generate plots,
histograms, power spectra, bar charts, errorcharts, scatterplots, etc, with just a few lines of
code.
"

LICENCES="PSF"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    cairo [[ description = [ add support for cairo  ] ]]
    gtk [[ description = [ build the gtk backend ] ]]
    gtk3 [[ description = [ build the gtk3 backend ] ]]
    latex [[ description = [ add support for LaTeX ] ]]
    qt4 [[ description = [ build the qt4 backend ] ]]
    qt5 [[ description = [ build the qt5 backend ] ]]
    tk [[ description = [ build the tk backend ] ]]
    webagg [[ description = [ build the web backend ] ]]
    wxwidgets [[ description = [ build the wxwidgets backend ] ]]
"

DEPENDENCIES="
    build+run:
        dev-python/numpy[>=1.5][python_abis:*(-)?]
        dev-python/pyparsing[>=1.5.6][python_abis:*(-)?]
        dev-python/python-dateutil[>=1.1][python_abis:*(-)?]
        dev-python/pytz[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        dev-python/tornado[python_abis:*(-)?]
        media-libs/freetype:2[>=2.4]
        media-libs/libpng:=[>=1.2]
        cairo? (
            dev-python/pycairo[python_abis:*(-)?]
        )
        gtk? (
            gnome-bindings/pygtk:2[>=2.2.0][python_abis:*(-)?]
        )
        gtk3? (
            gnome-bindings/pygobject:3[python_abis:*(-)?]
            x11-libs/gtk+:3[gobject-introspection]
        )
        latex? (
            app-text/dvipng
            app-text/ghostscript
            app-text/poppler
            dev-texlive/texlive-latex
        )
        qt4? (
            dev-python/PyQt4[python_abis:*(-)?]
        )
        qt5? (
            dev-python/PyQt5[python_abis:*(-)?]
        )
        tk? (
            dev-lang/tk[>=8.3]
        )
        webagg? (
            dev-python/tornado[python_abis:*(-)?]
        )
        wxwidgets? (
            dev-python/wxPython:*[python_abis:*(-)?]
        )
    suggestion:
        app-text/ghostscript [[
            description = [ required to test pdf generation in test suite ]
        ]]
        media-gfx/inkscape [[
            description = [ required to test svg generation in test suite ]
        ]]
"
# FIXME having inkscape installed doesn't test svg files as claimed by upstream
# maybe some option or extra package is missing?

# tests do all kind of funny things (e.g. connect to X)
# TODO: fix tests and add test=nose option to setup-py above
RESTRICT="test"

# from 1.3.1
#test_one_multibuild() {
#    # FIXME few test fails under python 2.7
#    PYTHONPATH=$(echo $(pwd)/build/lib*) edo ${PYTHON} -c "
#import sys, matplotlib as m
#sys.exit(0 if m.test(verbosity=2) else 1)
#    "
#}

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}/${PNV}-pkg-config.patch" )

