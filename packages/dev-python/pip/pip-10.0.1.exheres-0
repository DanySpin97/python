# Copyright 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools blacklist=none test=pytest ]

SUMMARY="Alternative Python package installer"
DESCRIPTION="
Pip installs packages. Python packages. An easy_install replacement.

Pip is a replacement for easy_install, and is intended to be an improved
package installer. It integrates with virtualenv, doesn't do partial
installs, can save package state for replaying, can install from non-egg
sources, and can install from version control repositories.
"
HOMEPAGE+=" https://pip.pypa.io"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
"

# Test-suite runs >20 minutes for each Python ABI.
# Nearly half of tests want to download stuff.
RESTRICT="test"

install_one_multibuild() {
    setup-py_install_one_multibuild

    edo sed \
        -e "s/python${MULTIBUILD_TARGET}/python/" \
        -i "${IMAGE}"/usr/$(exhost --target)/bin/pip
}

