# Copyright 2009 Jan Meier
# Copyright 2009, 2013 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools test=pytest ]

SUMMARY="Sphinx is a tool that makes it easy to create intelligent and beautiful documentation"
HOMEPAGE+=" http://sphinx-doc.org"

LICENCES="
    BSD-2       [[ note = [ smartypants.py, Sphinx ] ]]
    BSD-3       [[ note = [ SmartyPants_ ] ]]
    ElementTree [[ note = [ ElementTree ] ]]
    MIT         [[ note = [ JQuery ] ]]
    PSF-2.2     [[ note = [ pgen2 ] ]]
"
SLOT="0"
MYOPTIONS="
    ( providers: graphicsmagick imagemagick ) [[
        *description = [ Tests require the convert command ]
        number-selected = at-least-one
    ]]
"

DEPENDENCIES="
    build+run:
        dev-python/Babel[>=1.3][python_abis:*(-)?]
        dev-python/Jinja2[>=2.3][python_abis:*(-)?]
        dev-python/Pygments[>=2.0][python_abis:*(-)?]
        dev-python/alabaster[>=0.7&<0.8][python_abis:*(-)?]
        dev-python/docutils[>=0.11][python_abis:*(-)?]
        dev-python/imagesize[python_abis:*(-)?]
        dev-python/packaging[python_abis:*(-)?]
        dev-python/requests[>=2.0.0][python_abis:*(-)?]
        dev-python/six[>=1.5][python_abis:*(-)?]
        dev-python/snowballstemmer[>=1.1][python_abis:*(-)?]
        dev-python/sphinxcontrib-websupport[<1.1.0][python_abis:*(-)?]
        python_abis:2.7? ( dev-python/typing[python_abis:2.7] )
        python_abis:3.4? ( dev-python/typing[python_abis:3.4] )
    test:
        dev-python/SQLAlchemy[python_abis:*(-)?]
        dev-python/flake8[>=3.5.0][python_abis:*(-)?]
        dev-python/flake8-import-order[python_abis:*(-)?]
        dev-python/html5lib[python_abis:*(-)?]
        dev-python/mock[python_abis:*(-)?]
        dev-python/pytest-cov[python_abis:*(-)?]
        providers:graphicsmagick? ( media-gfx/GraphicsMagick[imagemagick] )
        providers:imagemagick? ( media-gfx/ImageMagick )
        python_abis:2.7? ( dev-python/enum34[python_abis:2.7] )
        python_abis:3.4? (
            dev-python/mypy[python_abis:3.4]
            dev-python/typed-ast[python_abis:3.4]
        )
        python_abis:3.5? (
            dev-python/mypy[python_abis:3.5]
            dev-python/typed-ast[python_abis:3.5]
        )
        python_abis:3.6? (
            dev-python/mypy[python_abis:3.6]
            dev-python/typed-ast[python_abis:3.6]
        )
    suggestion:
        dev-texlive/texlive-latexextra [[
            description = [ Use the LaTeX builder to create PDF documentation ]
            note = [ framed, threeparttable, titlesec, wrapfig ]
        ]]
"

prepare_one_multibuild() {
    default

    # fetch from the internet
    edo rm tests/test_build_{latex,linkcheck}.py

    # remove tests that require an already install Sphinx, last checked: 1.7.5
    edo rm tests/test_setup_command.py
}

test_one_multibuild() {
    # NOTE(somasis): UnicodeEncodeError failures with LC_ALL=C
    LC_ALL=en_US.UTF-8 \
    SPHINX_TEST_TEMPDIR="${TEMP}"\tests \
    default
}

